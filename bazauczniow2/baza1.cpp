#include "baza.h"

void wpisz(dziekanat *var)
{
    cin.get();
    cout<<"Podaj nazwisko ucznia: ";  cin>>var->nazwisko;
    cout<<"Podaj kierunek ucznia: ";   cin>>var->kierunek;
    cout<<"Podaj semestr ucznia: ";    cin>>var->semestr;
    cout<<"Podaj srednia ucznia: ";    cin>>var->srednia;
}
void sortujsemestr(dziekanat *var,int n)
{
    for(int j=0; j<n; j++)
    {
        int min=j;
        for(int k=j+1; k<n; k++)
        {
            if(var[k].semestr==var[min].semestr)
            {
                if(var[min].srednia<var[k].srednia)
                    min=k;
            }
            else if(var[k].semestr<var[min].semestr)
                min=k;
        }
        swap(var[j],var[min]);
    }
}
void wypisz(dziekanat *var)
{
    cout<<"Nazwisko: "<<var->nazwisko<<endl
        <<"Kierunek: "<<var->kierunek<<endl
        <<"Semestr: "<<var->semestr<<endl
        <<"Srednia ocen: "<<var->srednia<<endl<<endl;
}
int brak(int n)
{
    if(n==0)
    {
        cout<<"Brak uczniow w bazie, operacja niedozwolona!!\n\n";
        return 1;
    }
    else return 0;
}
void sortujkierunek(dziekanat *var,int n)
{
    for(int j = n - 1; j > 0; j--)
    {
        int p = 1;
        for(int i = 0; i < j; i++)
        {
            if(var[i].kierunek > var[i + 1].kierunek){
                swap(var[i], var[i + 1]);
                p = 0;
            }
            else if(var[i].kierunek == var[i + 1].kierunek)
            {
                if(var[i].semestr==var[i+1].semestr)
                {
                    if(var[i].nazwisko > var[i+1].nazwisko)
                    swap(var[i], var[i + 1]);
                    p = 0;
                }
                else if(var[i].semestr<var[i+1].semestr){
                    swap(var[i], var[i + 1]);
                    p = 0;
                }
            }
        }
        if(p) break;
    }
}
