#ifndef BAZA_H_INCLUDED
#define BAZA_H_INCLUDED
#include <iostream>
using namespace std;

struct dziekanat
{
    double srednia;
    string kierunek;
    int semestr;
    string nazwisko;
};

int brak(int n);
void wpisz(dziekanat *var);
void sortujsemestr(dziekanat *var,int n);
void sortujkierunek(dziekanat *var,int n);
void wypisz(dziekanat *var);

#endif // BAZA_H_INCLUDED
