// stacker.cpp -- test klasy Stack
#include <iostream>
#include <cctype>                // albo ctype.h
#include "stack.h"
int main()
{
    using namespace std;
    Stack st,st2;                    // tworzy pusty stos
    char ch;
    unsigned long po;
    cout << "Naciśnij D, aby wprowadzić deklarację, \n"
        << "P, aby przetworzyć deklarację, lub K, aby zakończyć.\n";
    while (cin >> ch && toupper(ch) != 'K')
    {
        while (cin.get() != '\n')
            continue;
        if (!isalpha(ch))
        {
            cout << '\a';
            continue;
        }
        switch(ch)
        {
            case 'D':
            case 'd': cout << "Podaj numer nowej deklaracji: ";
                      cin >> po;

                      if (st.isfull())
                          cout << "Stos pełen!\n";
                      else
                      {
                            st.push(po);
                            //st2 = st;
                      }
                      break;
            case 'P':
            case 'p':
                      Stack st3 = st;
                      if (st3.isempty())
                          cout << "Stos pusty!\n";
                      else {
                          st3.pop(po);
                          cout << "Deklaracja nr" << po << " zdjęta\n";
                      }
                      break;
        }
        cout << "Naciśnij D, aby wprowadzić deklarację, \n"
            << "P, aby przetworzyć deklarację, lub K, aby zakończyć.\n";
    }
    cout << "Fajrant!\n";
    return 0;
}

