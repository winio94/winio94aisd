// stack.cpp -- metody klasy stosu
#include "stack.h"
#include<iostream>
Stack::Stack(int n)            // tworzy pusty stos
{
    size = n;
    pitems = new Item[size];
    top = 0;
}

Stack::Stack(const Stack & st)
{
std::cout<<"Konstruktor kopiujący!!\n";
    size = st.size;
    pitems = new Item[size];
    top = st.top;
    while(top >= 0)
    {
        pitems[top] = st.pitems[top];
        top--;
    }

}
Stack::~Stack()
{
    delete [] pitems;
}
bool Stack::isempty() const
{
    return top == 0;
}

bool Stack::isfull() const
{
    return top == MAX;
}

bool Stack::push(const Item & item)
{

    if (top < MAX)
    {
        pitems[top++] = item;
        size++;
        return true;
    }
    else
        return false;
}

bool Stack::pop(Item & item)
{
    if (top > 0)
    {
        item = pitems[--top];
        size--;
        return true;
    }
    else
        return false;
}

Stack & Stack::operator=(const Stack & st)
{
    std::cout<<"operator przypisania!\n";
    if(this == &st)
        return *this;
    delete [] pitems;
    pitems = new Item[st.size];
    top = st.top;

    while(top > 0)
    {
        pitems[top] = st.pitems[top];
        top--;
    }
    return *this;
}




