#include <iostream>

using namespace std;

struct BST
{
    BST * prawy;
    BST * lewy;
    int klucz;
};

BST * root = NULL;

void dodaj(int k);
void wyszukaj(int k);
void wyswietl(BST * r);

int main()
{
    dodaj(5);
    dodaj(3);
    dodaj(63);
    dodaj(31);
    wyswietl(root);
    wyszukaj(3);
    return 0;
}

void dodaj(int k)
{
    BST * p = new BST;
    p->klucz = k;
    p->prawy = NULL;
    p->lewy = NULL;

    if(root == NULL)
        root = p;
    else
    {
        BST * x = root;
        BST * m;//wskaznik do zapamietania pozycji

        while(x != NULL)
        {
            m = x;
            if(p->klucz < x->klucz)
                x = x->lewy;
            else
                x = x->prawy;
        }
        if(p->klucz < m->klucz)
            m->lewy = p;
        else
            m->prawy = p;
    }
}

void wyszukaj(int k)
{
    bool znaleziono = false;
    BST * p = root;
    if(p != NULL)
    {
        while((p != NULL) && (!znaleziono))
        {
            if(p->klucz == k)
                znaleziono = true;
            else
            {
                if(k < p->klucz)
                    p = p->lewy;
                else
                    p = p->prawy;
            }
        }
    }
    else cout<<"Drzewo nie ma zadnego wezla!\n";

    if(znaleziono)
        cout<<"W drzewie istnieje element o wartosci "<<k<<endl;
    else
        cout<<"Nie ma takiego elementu w drzewie!\n";
}

void wyswietl(BST * r)
{
    if(r)
    {
        wyswietl(r->lewy);
        cout<<r->klucz<<endl;
        wyswietl(r->prawy);
    }
}
