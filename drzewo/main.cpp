#include <iostream>
using namespace std;

struct bst
{
    bst *lewy;
    bst *prawy;
    int wartosc;
};
bst *root = NULL;

void wyswietl(bst *wsk)
{
//najpierw lewa galaz
    if (wsk->lewy!=NULL)
    {
        wyswietl(wsk->lewy);
    }
//potem srodek
    cout<<wsk->wartosc<<" ";
//potem prawa galaz
    if (wsk->prawy!=NULL)
    {
        wyswietl(wsk->prawy);
    }
}

void dodaj(bst *wsk, int y)
{
    if(wsk!=NULL)
    {
        if(y<=wsk->wartosc)
        {
            if(wsk->lewy==NULL)  //dodawanie z lewej strony
            {
                bst *poj_el = new bst;
                poj_el->wartosc=y;
                poj_el->lewy=NULL;
                poj_el->prawy=NULL;
                wsk->lewy=poj_el;
            }
            else
            {
                dodaj(wsk->lewy,y);
            }
        }
        if(y>wsk->wartosc)
        {
            if(wsk->prawy==NULL)  //dodawanie z prawej strony
            {
                bst *poj_el=new bst;
                poj_el->wartosc=y;
                poj_el->lewy=NULL;
                poj_el->prawy=NULL;
                wsk->prawy=poj_el;
            }
            else
            {
                dodaj(wsk->prawy,y);
            }
        }
    }
    else  //dodawanie elementu na poczatku (drzewo bylo puste)
    {
        bst *poj_el=new bst;
        poj_el->wartosc=y;
        poj_el->lewy=NULL;
        poj_el->prawy=NULL;
        root=poj_el;
    }
}

void usun(bst *wsk)
{
    if(wsk!=NULL)
    {
        if(wsk->lewy!=NULL)
        {
            usun(wsk->lewy);
        }
        if(wsk->prawy!=NULL)
        {
            usun(wsk->prawy);
        }
        delete wsk;
    }
    else
    {
        cout<<"\n Drzewo jest puste";
    }
}

int main()
{

    while(1)
    {
        cout<<"\n Dodawanie (1) ---===--- Usuwanie (2) \n";
        int zm;
        cin>>zm;
        if(zm==1)
        {
            cout<<"Podaj liczbe: \n";
            int x;
            cin>>x;
            dodaj(root, x);
            wyswietl(root);
        }
        if (zm==2)
        {
            usun(root);
        }
    }

    cin.get();
    cin.ignore();
}
