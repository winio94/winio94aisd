#include <iostream>

using namespace std;

struct kolejka
{
    kolejka * next;
    int dane;
};

//tworze pustą kolejke
kolejka * head = NULL;
int c = 0;
//funkcje
void enquene(int n);
void dequene();
void show();

int main()
{
    enquene(5);
    enquene(1);
    enquene(7);
    enquene(53);
    enquene(25);
    enquene(1235);


    dequene();
    dequene();

    show();

    return 0;
}
void enquene(int n)
{
    //licznik elemnetow
    ++c;

    kolejka * p = new kolejka;//tworze nowy element;
    p->dane = n;

    //dodajemy na koniec;
    kolejka * x = head;

    if(x != NULL)
    {
        while(x->next != NULL)
            x = x-> next;

        p->next = NULL;
        x->next = p;

    }
    else
    {
        p->next = NULL;
        head = p;
    }

}
void dequene()
{
    --c;
    kolejka * p = head;
    head = p->next;
    delete p;
}

void show()
{
    kolejka * p = head;

    while(p)
    {
        cout<< p->dane <<endl;
        p = p->next;
    }
}
