#include <cstdlib>
#include <iostream>
using namespace std;

int const N = 10;
struct Student
{
    string nazwisko;
    float srednia;
};

void zadanie1()
{

    int dane[N] = {7,3,5,1,9,3,8,2,6,4};
    for(int i = 0 ; i < N; i++)
        cout<<dane[i]<<" ";
    for(int i = 0 ; i < N -1; i++)
    {
        for(int j = 0; j < N - 1; j++)
            if(dane[j] <= dane[j+1])
                swap(dane[j],dane[j+1]);
    }
    cout<<endl;
    for(int i = 0 ; i < N; i++)
        cout<<dane[i]<<" ";
}

void zadanie2a()
{
    Student dane[N] =
    {
        {"Katowicki", 3.0},
        {"Nyski", 4.5},
        {"Augustowski", 4.5},
        {"Krakowski", 5.0},
        {"Opolski", 3.5},
        {"Wroclawski", 4.0},
        {"Poznanski", 4.0},
        {"Krakowski" , 4.5},
        {"Radomski", 3.5},
        {"Katowicki", 4.0}
    };
    float mark;
    cout<<"\nPodaj srednia: ";
    cin>>mark;
    for(int i = 0 ; i < N; i++)
        if(dane[i].srednia > mark)
            cout<<dane[i].nazwisko<<" "<<dane[i].srednia<<endl;

}

void zadanie2b()
{

    Student dane[N] =
    {
        {"Katowicki", 3.0},
        {"Nyski", 4.5},
        {"Augustowski", 4.5},
        {"Krakowski", 5.0},
        {"Opolski", 3.5},
        {"Wroclawski", 4.0},
        {"Poznanski", 4.0},
        {"Krakowski" , 4.5},
        {"Radomski", 3.5},
        {"Katowicki", 4.0}
    };
    cout<<"\nPrzed sortowaniem:\n";
    for(int i = 0 ; i < N; i++)
        cout<<dane[i].nazwisko<<" "<<dane[i].srednia<<endl;

    int min;
    for(int i = 0 ; i < N; i++)
    {
        min = i;
        for(int j = i + 1 ; j < N ; j++)
        {
            if(dane[j].nazwisko < dane[min].nazwisko)
                min = j;
        }
        swap(dane[i],dane[min]);
    }
    cout<<"\nPo sortowaniem:\n";
    for(int i = 0 ; i < N; i++)
        cout<<dane[i].nazwisko<<" "<<dane[i].srednia<<endl;

}


void zadanie3()
{
    Student dane[N] =
    {
        {"Katowicki", 3.0},
        {"Nyski", 4.5},
        {"Augustowski", 4.5},
        {"Krakowski", 5.0},
        {"Opolski", 3.5},
        {"Wroclawski", 4.0},
        {"Poznanski", 4.0},
        {"Krakowski" , 4.5},
        {"Radomski", 3.5},
        {"Katowicki", 4.0}
    };
    cout<<"\nPrzed sortowaniem:\n";
    for(int i = 0 ; i < N; i++)
        cout<<dane[i].nazwisko<<" "<<dane[i].srednia<<endl;

    for(int i = 0 ; i < N; i++)
    {
        for(int j = 0 ; j < N - 1; j++)
        {
            if(dane[j].srednia < dane[j + 1].srednia)
                swap(dane[j],dane[j + 1]);
            else if(dane[j].srednia == dane[j + 1].srednia)
            {
                if(dane[j].nazwisko >= dane[j + 1].nazwisko)
                    swap(dane[j],dane[j + 1]);
            }
        }
    }
    cout<<"\nPo sortowaniem:\n";
    for(int i = 0 ; i < N; i++)
        cout<<dane[i].nazwisko<<" "<<dane[i].srednia<<endl;

}


int main(int argc, char *argv[])
{
    zadanie1();
    zadanie2a();
    zadanie2b();
    zadanie3();

    return 0;
}
