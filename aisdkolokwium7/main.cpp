#include <cstdlib>
#include <iostream>
using namespace std;

int const N = 10;
struct Produkt
    {
        string producent;
        string produkt;
    };

void zadanie1()
{
    int dane[N] = {7,3,5,1,9,3,8,2,6,4};
    for(int i = 0 ; i < N ; i++)
        cout<<dane[i]<<" ";
    cout<<endl;

    int min;
    for(int i = 0 ; i < N; i++)
    {
        min = i;
        for(int j = i + 1 ; j < N   ; j++)
        {
            if(dane[j] <= dane[min])
                min = j;
        }
        swap(dane[i],dane[min]);
    }
    for(int i = 0 ; i < N ; i++)
        cout<<dane[i]<<" ";
    cout<<endl;
}

void zadanie2a()
{
    Produkt dane[N]=
    {
        { "ePaper","teczkaA4" },
        { "ABC","notesA560" },
        { "ePaper","zeszyt100l" },
        { "Goose","pioro" },
        { "Hen","pioro" },
        { "PP","notesA560" },
        { "ABC","zeszyt160k" },
        { "ABC","zeszyt160l" },
        { "PP","teczkaA4" },
        { "Goose","pioro" }
    };
    string name;
    cout<<"Podaj nazwe producenta:";
    cin>>name;
    for(int i = 0 ; i < N; i++)
        if(dane[i].producent == name)
            cout<<dane[i].produkt<<endl;

}

void zadanie2b()
{
    Produkt dane[N]=
    {
        { "ePaper","teczkaA4" },
        { "ABC","notesA560" },
        { "ePaper","zeszyt100l" },
        { "Goose","pioro" },
        { "Hen","pioro" },
        { "PP","notesA560" },
        { "ABC","zeszyt160k" },
        { "ABC","zeszyt160l" },
        { "PP","teczkaA4" },
        { "Goose","pioro" }
    };
    cout<<"\nPrzed sortowaniem:\n";
    for(int i = 0 ; i < N ; i++)
        cout<<dane[i].producent<<" "<<dane[i].produkt<<endl;
    cout<<endl;

    int j;
    Produkt var;
    for(int i = 0 ; i < N; i++)
    {
        var = dane[i];
        j = i - 1;
        while((j >= 0) && (dane[j].produkt > var.produkt))
        {
            dane[j + 1] = dane[j];
            j = j - 1;
        }
        dane[j+1] = var;
    }
    cout<<"\nPo sortowaniem:\n";
    for(int i = 0 ; i < N ; i++)
        cout<<dane[i].producent<<" "<<dane[i].produkt<<endl;
    cout<<endl;

}

void zadanie3()
{
    Produkt dane[N]=
    {
        { "EPaper","teczkaA4" },
        { "ABC","notesA560" },
        { "EPaper","zeszyt100l" },
        { "Goose","pioro" },
        { "Hen","pioro" },
        { "PP","notesA560" },
        { "ABC","zeszyt160k" },
        { "ABC","zeszyt160l" },
        { "PP","teczkaA4" },
        { "Goose","pioro" }
    };
    cout<<"\nPrzed sortowaniem:\n";
    for(int i = 0 ; i < N ; i++)
        cout<<dane[i].producent<<" "<<dane[i].produkt<<endl;
    cout<<endl;

    int min;
    for(int i = 0 ;i < N; i++)
    {
        min = i;
        for(int j = i + 1; j < N ; j++)
        {
            if(dane[j].produkt < dane[min].produkt)
                min = j;
            else if(dane[j].produkt == dane[min].produkt)
            {
                if(dane[j].producent < dane[min].producent)
                    min = j;
            }
        }
        swap(dane[i],dane[min]);
    }
     cout<<"\nPo sortowaniem:\n";
    for(int i = 0 ; i < N ; i++)
        cout<<dane[i].producent<<" "<<dane[i].produkt<<endl;
    cout<<endl;

}

int main(int argc, char *argv[])
{
    //zadanie1();
    //zadanie2a();
    //zadanie2b();
    zadanie3();

    return 0;
}
