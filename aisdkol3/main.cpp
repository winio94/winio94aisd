#include <cstdlib>
#include <iostream>
#include<string>
using namespace std;

int const N = 10;
struct Student
{
    string nazwisko;
    float ocena;
};

void zadanie1();
void zadanie2();


int main(int argc, char *argv[])
{
    //cout<<"Zadanie 1"<<endl;
   // zadanie1();

    cout<<"Zadanie 2"<<endl;
    zadanie2();


    return 0;
}
void zadanie1()
{
    //tablica struktur
    Student dane[N]=
    {
        {"Katowicki" , 3.0},
        {"Nyski", 4.5},
        {"Augustowski", 4.5},
        {"Krakowski", 5.0},
        {"Opolski", 3.5},
        {"Wroclawski", 4.0},
        {"Poznanski", 4.0},
        {"Krakowski", 4.5},
        {"Radomski", 3.5},
        {"Katowicki", 4.0}
    };
    float mark;
    cout<<"Podaj ocene: ";
    cin>>mark;
    for(int i = 0 ; i < N; i++)
    {
        if(dane[i].ocena > mark)
            cout<<dane[i].nazwisko<<" "<<dane[i].ocena<<endl;
    }
}

void zadanie2()
{
    Student dane[N]=
    {
        {"Katowicki" , 3.0},
        {"Nyski", 4.5},
        {"Augustowski", 4.5},
        {"Krakowski", 5.0},
        {"Opolski", 3.5},
        {"Wroclawski", 4.0},
        {"Poznanski", 4.0},
        {"Krakowski", 4.5},
        {"Radomski", 3.5},
        {"Katowicki", 4.0}
    };
    cout<<"Przed sortowaniem:\n\n";
    for(int i = 0 ; i < N; i++)
        cout<<dane[i].nazwisko<<" "<<dane[i].ocena<<endl;

    cout<<"Po posortowaniu:\n\n";
    int j;
    Student var;
    for(int i = 1; i < N; i++)
    {
        var = dane[i];
        j = i - 1;
        while((j>=0) && var.ocena > dane[j].ocena )
        {
            dane[j+1] = dane[j];
            j = j - 1;
        }
        dane[j+1] = var;
    }
    for(int i = 0 ; i < N; i++)
        cout<<dane[i].nazwisko<<" "<<dane[i].ocena<<endl;

}
