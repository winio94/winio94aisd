#include <iostream>

using namespace std;

struct Stos
{
    Stos * next;
    int dane;
};

Stos * head = NULL;
int c = 0;

void push(int n);
void pop();
void show();

int main()
{
    push(5);
    push(4);
    push(2);
    push(75);
    pop();


    show();
    return 0;
}

void push(int n)
{
    ++c;
    Stos * p = new Stos;
    p->dane = n;

    if(head != NULL)
    {
        p->next = head;
        head = p;
    }
    else
        head = p;
}
void pop()
{
    Stos * p = head;
    head = p->next;
    delete p;
}

void show()
{
    Stos* p = head;
    while(p!=NULL)
    {
        cout<<p->dane<<endl;
        p=p->next;

    }
}

