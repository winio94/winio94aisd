#include<string>
#include <iostream>
using namespace std;

int const N = 5;
    int const M = 6;
    int const S = N + M;
//tablica zainicjalizowana danymi podanymi w zadaniu – ciąg 1
//tablica zainicjalizowana danymi podanymi w zadaniu – ciąg 2
    string tab1[N] {"augustowski","katowicki", "krakowski", "nyski", "opolski" };
    string tab2[M] {"krakowski", "lubelski", "opolski", "poznanski", "radomski", "wroclawski" };
//tablica wynikowa
    string tab3[S];

void merges(int pocz, int sr, int kon)
{
    int i, j , q;
    i = 1;
    j = 1;
    for(int k = pocz; k < kon; k++)
    {
        if(tab1[i] <= tab2[j])
        {
            tab3[k] = tab1[i];
            i++;
        }
        else
        {
            tab3[k] = tab2[j];
            j++;
        }
    }
}
void mergesort(int pocz, int kon)
{
    int sr;
    if(pocz < kon)
    {
        sr = (pocz + kon)/2;
        mergesort(pocz, sr);
        mergesort(sr + 1, kon);
        merges(pocz, sr, kon);
    }
}

int main()
{
    mergesort(0, S-1);

    for(int i = 0; i < S; i++)
        cout<<tab3[i]<<"\n";
    cout<<"\n\n";

    return 0;
}
