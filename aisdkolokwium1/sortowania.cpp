#include "sortowania.h"

void zamiana()
{
    int const N = 10;
    //tablica stringow
    string tab1[N] { "katowicki","nyski","augustowski","krakowski","opolski",
                     "wroclawski","poznanski","krakowski","radomski","katowicki"
                   };
    //sortowanie bąbelkowe polega na wielokrotnym powtarzaniu
    // operacji zamiany sąsiednich elementów
    //które są w niewłaściwej kolejności
    for(int i = 0; i < N; i++)
        cout<<tab1[i]<<"\n";
    cout<<"\n\n";
    for(int  i = 0; i < N  ; i++)
    {
        for(int j = 0; j < N - 1 ; j++)
        {
            string var="";
            if(tab1[j] >= tab1[j+1])
                swap(tab1[j], tab1[j+1]);
        }
    }
    for(int i = 0; i < N; i++)
        cout<<tab1[i]<<"\n";
    cout<<"\n\n";

}


void wstawianie()
{
    int const N = 10;
    //tablica stringow
    string tab1[N] { "katowicki","nyski","augustowski","krakowski","opolski",
                     "wroclawski","poznanski","krakowski","radomski","katowicki"
                   };
    //sortowanie bąbelkowe polega na wielokrotnym powtarzaniu
    // operacji zamiany sąsiednich elementów
    //które są w niewłaściwej kolejności
    for(int i = 0; i < N; i++)
        cout<<tab1[i]<<"\n";
    cout<<"\n\n";

    string var = "";
    for(int j = 1; j < N; j++)
    {
        var = tab1[j];
        int  i = j - 1;
        while(i >= 0 &&  tab1[i] >= var)
        {
            tab1[i+1] = tab1[i];
            i = i - 1;
        }
        tab1[i + 1] = var;
    }

    for(int i = 0; i < N; i++)
        cout<<tab1[i]<<"\n";
    cout<<"\n\n";
}

void wybor()
{
    int const N = 10;
    //tablica stringow
    string tab1[N] { "katowicki","nyski","augustowski","krakowski","opolski",
                     "wroclawski","poznanski","krakowski","radomski","katowicki"
                   };
    //sortowanie bąbelkowe polega na wielokrotnym powtarzaniu
    // operacji zamiany sąsiednich elementów
    //które są w niewłaściwej kolejności
    for(int i = 0; i < N; i++)
        cout<<tab1[i]<<"\n";
    cout<<"\n\n";

    int min;
    for(int i = 0; i < N - 1 ; i++)
    {
        min = i;
        for(int j = i + 1 ; j < N ; j++)
        {
            if(tab1[j] < tab1[min])
                min = j;
        }
        swap(tab1[i], tab1[min]);
    }

    for(int i = 0; i < N; i++)
        cout<<tab1[i]<<"\n";
    cout<<"\n\n";
}


