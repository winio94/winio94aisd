#include <iostream>
#include<cstdlib>
using namespace std;
struct dziekanat
{
    double srednia;
    string kierunek;
    int semestr;
    string nazwisko;
};
int brak(int n);
void wpisz(dziekanat *var);
void sortujsemestr(dziekanat *var,int n);
void sortujkierunek(dziekanat *var,int n);
void wypisz(dziekanat *var);
int main()
{
    int wybor,ilosc=0;
    dziekanat * pk;
    while(1)
    {
        cout<<"\t\t\tWybierz co chcesz zrobic\n";
        cout<<"1. wpisz ucznia/uczniow do bazy:\n";
        cout<<"2. sortuj uczniow wedlug semestru->sredniej.\n"
            <<"3. sortuj uczniow wedlug kierunku->semestru->nazwiska.\n"
            <<"4. wypisz posortowanych uczniow:\n\n";
        cin>>wybor;
        switch(wybor)
        {
        case 1:
            cout<<"Ile uczniow chcesz dodac do bazy?\nIlosc: ";
            cin>>ilosc;
            pk=new dziekanat[ilosc];
            for(int i=0; i<ilosc; i++){
                cout<<"Uczen nr."<<i+1<<"\n";
                wpisz(pk+i);
            }
            break;
        case 2:
            if( brak(ilosc) )   break;
            else
                sortujsemestr(pk,ilosc);
            break;
        case 3:
            if( brak(ilosc) )    break;
            else
                sortujkierunek(pk,ilosc);
            break;
        case 4:
            if( brak(ilosc) )    break;
            else{
                for(int i=0; i<ilosc; i++){
                    cout<<"Uczen nr."<<i+1<<"\n";
                    wypisz(pk+i);
                }
            }
            break;
        default:
            cout<<"Do widzenia!";
            return 0;
        };
    }
    return 0;
}
void wpisz(dziekanat *var)
{
    cin.get();
    cout<<"Podaj snazwisko ucznia: ";  cin>>var->nazwisko;
    cout<<"Podaj kierunek ucznia: ";   cin>>var->kierunek;
    cout<<"Podaj semestr ucznia: ";    cin>>var->semestr;
    cout<<"Podaj srednia ucznia: ";    cin>>var->srednia;
}
void sortujsemestr(dziekanat *var,int n)
{
    for(int j=0; j<n; j++)
    {
        int min=j;
        for(int k=j+1; k<n; k++)
        {
            if(var[k].semestr==var[min].semestr)
            {
                if(var[min].srednia<var[k].srednia)
                    min=k;
            }
            else if(var[k].semestr<var[min].semestr)
                min=k;
        }
        swap(var[j],var[min]);
    }
}
void wypisz(dziekanat *var)
{
    cout<<"Nazwisko: "<<var->nazwisko<<endl
        <<"Kierunek: "<<var->kierunek<<endl
        <<"Semestr: "<<var->semestr<<endl
        <<"Srednia ocen: "<<var->srednia<<endl<<endl;
}
int brak(int n)
{
    if(n==0)
    {
        cout<<"Brak uczniow w bazie, operacja niedozwolona!!\n\n";
        return 1;
    }
    else return 0;
}
void sortujkierunek(dziekanat *var,int n)
{
    for(int j = n - 1; j > 0; j--)
    {
        int p = 1;
        for(int i = 0; i < j; i++)
        {
            if(var[i].kierunek > var[i + 1].kierunek){
                swap(var[i], var[i + 1]);
                p = 0;
            }
            else if(var[i].kierunek == var[i + 1].kierunek)
            {
                if(var[i].semestr==var[i+1].semestr)
                {
                    if(var[i].nazwisko > var[i+1].nazwisko)
                    swap(var[i], var[i + 1]);
                    p = 0;
                }
                else if(var[i].semestr<var[i+1].semestr){
                    swap(var[i], var[i + 1]);
                    p = 0;
                }
            }
        }
        if(p) break;
    }
}
