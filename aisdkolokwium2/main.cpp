#include <cstdlib>
#include <iostream>
using namespace std;
struct baza
{
    string student;
    float ocena;
};

void zadanie2()
{
    baza lista[10]=
    {
        {"katowicki",3.0},
        {"nyski",4.5},
        {"augustowski",4.5},
        {"krakowski",5.0},
        {"opolski",3.5},
        {"wroclawski",4.0},
        {"poznanski",4.0},
        {"krakowski",4.5},
        {"radomski",3.5},
        {"katowicki",4.0}
    };
    int min;
    for(int i = 0 ; i < 10; i++)
    {
        min = i;
        for(int j = i + 1 ; j < 10; j++)
        {
            if(lista[j].student < lista[min].student)
                min = j;
        }
        swap(lista[i].student,lista[min].student);
    }
    for(int i=0;i<10;i++)
    {
            cout<<lista[i].student<<" ";

    }
}
int main(int argc, char *argv[])
{

 cout<<"Zadanie 2"<<endl;
 zadanie2();
 return EXIT_SUCCESS;
}
