#include<string>
#include <cstdlib>
#include <iostream>
using namespace std;

int const N = 10;
struct Kraj
{
    string kontynent;
    string kraj;
    float powierzchnia;
};

void zadanie2(Kraj d[], int lewy, int prawy);

int main()
{

    Kraj dane[N] =
    {
        {"Europa", "Polska", 312.685 },
        {"Azja", "Chiny", 9597.000 },
        {"Afryka", "Egipt", 1001.450 },
        {"AmerykaPl", "Meksyk", 1972.550 },
        {"AmerykaPn", "Kanada", 9976.140 },
        {"Azja", "Indie", 3287.262 },
        {"Azja", "Wietnam", 331.688 },
        {"Europa", "Norwegia", 324.220 },
        {"AmerykaPl", "Kolumbia", 1141.748 },
        {"Europa", "Niemcy", 357.021 }
    };

     cout<<"przed posortowaniem:\n";
    for(int i = 0 ;  i < N; i++)
        cout<<dane[i].kontynent<<" "<<dane[i].kraj<<" "<<dane[i].powierzchnia<<endl;

    zadanie2(dane, 0 , N - 1);

    cout<<endl<<endl<<"po posortowaniu:\n";
    for(int i = 0 ;  i < N; i++)
        cout<<dane[i].kontynent<<" "<<dane[i].kraj<<" "<<dane[i].powierzchnia<<endl;


    return 0;
}

void zadanie2(Kraj d[], int lewy, int prawy)
{
    int i, j ;
    Kraj piwot;
    i = (lewy + prawy)/ 2;
    piwot = d[i];
    d[i] = d[prawy];

    for(i = j = lewy; i < prawy; i++)
    {
        if(d[i].kontynent < piwot.kontynent)
        {
            swap(d[i], d[j]);
            j++;
        }
    }
    d[prawy] = d[j];
    d[j] = piwot;

    if(lewy < j - 1) zadanie2(d, lewy, j-1);
    if(j + 1 < prawy) zadanie2(d,j +1, prawy);

}













